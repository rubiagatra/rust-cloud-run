# Using Rust Toolchain
# FROM rust:1.62.0
 
# WORKDIR /rust-cloud-run
# COPY . .
 
# RUN cargo install --path .
 
# CMD ["rust-cloud-run"]

# Using Distroless
FROM rust:1.62.0 as build-env
WORKDIR /rust-cloud-run
COPY . /rust-cloud-run
RUN cargo build --release

FROM gcr.io/distroless/cc
COPY --from=build-env /rust-cloud-run/target/release/rust-cloud-run /
CMD ["./rust-cloud-run"]
